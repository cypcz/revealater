import * as functions from "firebase-functions";
import { db } from "../utils/init";

export const onRegister = functions
  .region("europe-west1")
  .auth.user()
  .onCreate(async (user, context) => {
    try {
      await db
        .collection("users")
        .doc(user.uid)
        .set({});
      return;
    } catch (error) {
      console.log(error);
      return;
    }
  });

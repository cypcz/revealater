import * as functions from "firebase-functions";
import { db } from "../utils/init";

export const getContent = functions.region("europe-west1").https.onCall(async (data, context) => {
  const doc = await db
    .collection("tasks")
    .doc(data.id)
    .get();

  const docData = doc.data();

  if (!docData) {
    return {
      data: null,
      error: "Document does not exist.",
    };
  }

  if (docData.password !== data.password) {
    return {
      data: null,
      error: "Wrong password.",
    };
  }

  if (docData.notifyOn.length > 0 || docData.toNotifyCount > 0) {
    return {
      data: { notifyOn: docData.notifyOn[docData.notifyOn.length - 1] },
      error: "Data not available yet.",
    };
  }

  return {
    data: {
      from: docData.from,
      to: docData.to,
      subject: docData.subject,
      message: docData.message,
      fileUrl: docData.fileUrl,
    },
    error: null,
  };
});

import * as functions from "firebase-functions";
import { db } from "../utils/init";

export const getMyTasks = functions.region("europe-west1").https.onCall(async (data, context) => {
  const snapshot = await db
    .collection("tasks")
    .where("author", "==", context.auth?.uid)
    .select("fileUrl", "message", "to", "notifiedOn", "notifyOn", "subject")
    .get();

  const tasks: any = [];

  snapshot.forEach(doc => tasks.push({ id: doc.id, ...doc.data() }));

  return {
    data: tasks,
    error: null,
  };
});

import * as functions from "firebase-functions";
import { auth, db, stripe } from "../utils/init";

export const cancelSubscription = functions
  .region("europe-west1")
  .https.onCall(async (data, context) => {
    try {
      if (!context.auth?.uid) {
        throw new Error("Not authenticated");
      }

      const userSnapshot = await db
        .collection("users")
        .doc(context.auth.uid)
        .get();
      const userData = userSnapshot.data();

      if (userData) {
        const stripeUser = await stripe.customers.retrieve(userData.stripeId);
        if (!stripeUser.deleted) {
          if (stripeUser.subscriptions?.data.length) {
            await stripe.subscriptions.del(stripeUser.subscriptions.data[0].id);
            const user = await auth.getUser(context.auth.uid);
            await auth.setCustomUserClaims(user.uid, { ...user.customClaims, isSubscribed: false });
          } else {
            return {
              data: null,
              error: {
                type: "Your subscription has already been canceled.",
              },
            };
          }
        }
      }

      return {
        data: null,
        error: null,
      };
    } catch (error) {
      console.log(error);
      return {
        data: null,
        error,
      };
    }
  });

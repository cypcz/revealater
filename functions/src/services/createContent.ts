import * as functions from "firebase-functions";
import { generatePassword } from "../utils/generatePassword";
import { db } from "../utils/init";

export const createContent = functions
  .region("europe-west1")
  .https.onCall(async (data, context) => {
    try {
      await db.collection("tasks").add({
        notifyOn: data.notifyOn.map(date => new Date(date)),
        notifiedOn: [],
        from: data.from,
        to: data.to,
        message: data.message,
        subject: data.subject,
        fileUrl: data.fileUrl,
        password: generatePassword(),
        author: context.auth ? context.auth.uid : null,
        toNotifyCount: data.notifyOn.length,
      });
      return {
        data,
        error: null,
      };
    } catch (error) {
      console.log(error);
      return {
        data: null,
        error,
      };
    }
  });

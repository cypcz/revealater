import * as admin from "firebase-admin";
import * as functions from "firebase-functions";
import mailgun from "mailgun-js";
import { config } from "../config";
import { db, mg } from "../utils/init";

export const taskRunner = functions
  .region("europe-west1")
  .pubsub.schedule("*/5 * * * *")
  .onRun(async context => {
    const now = admin.firestore.Timestamp.now();
    const tasks = await db
      .collection("tasks")
      .where("toNotifyCount", ">", 0)
      .get();

    const jobs: Promise<any>[] = [];

    tasks.forEach(snapshot => {
      snapshot.data().notifyOn.forEach(dueDate => {
        if (dueDate.toMillis() <= now.toMillis()) {
          const data = snapshot.data();
          const emailData:
            | mailgun.messages.SendData
            | mailgun.messages.BatchData
            | mailgun.messages.SendTemplateData =
            data.toNotifyCount > 1
              ? {
                  from: "Revealater <hello@revealater.com>",
                  to: data.to,
                  subject: `There was a content scheduled for you${
                    data.from ? ` by ${data.from}` : ""
                  }!`,
                  template: "intermediate_notification",
                  "h:X-Mailgun-Variables": JSON.stringify({
                    contentUrl: `${config.env.domain}/app/reveal/${snapshot.id}`,
                    password: data.password,
                  }),
                }
              : {
                  from: "Revealater <hello@revealater.com>",
                  to: data.to,
                  subject: `There was a content scheduled for you${
                    data.from ? ` by ${data.from}` : ""
                  }!`,
                  template: "last_notification",
                  "h:X-Mailgun-Variables": JSON.stringify({
                    contentUrl: `${config.env.domain}/app/reveal/${snapshot.id}`,
                    password: data.password,
                  }),
                };

          const promise = mg
            .messages()
            .send(emailData)
            .then(() =>
              snapshot.ref
                .update({
                  notifyOn: data.notifyOn.filter(date => date.toMillis() != dueDate.toMillis()),
                  notifiedOn: [...data.notifiedOn, dueDate.toDate()],
                  toNotifyCount: data.toNotifyCount - 1,
                })
                .catch(err => {
                  console.log(err);
                  snapshot.ref.update({ status: "error" });
                })
            )
            .catch(err => {
              console.log(err);
              return snapshot.ref.update({ status: "error" });
            });
          jobs.push(promise);
        }
      });
    });

    return await Promise.all(jobs);
  });

import * as functions from "firebase-functions";
import Stripe from "stripe";
import { config } from "../config";
import { db, stripe } from "../utils/init";

export const createSubscription = functions
  .region("europe-west1")
  .https.onCall(async (data, context) => {
    try {
      if (!context.auth?.uid) {
        throw new Error("Not authenticated");
      }

      const userDoc = await db
        .collection("users")
        .doc(context.auth.uid)
        .get();
      const userData = userDoc.data();

      let customer: Stripe.Customer | undefined;

      if (!userData?.stripeId) {
        customer = await stripe.customers.create({
          payment_method: data.paymentMethod?.id,
          email: data.paymentMethod?.billing_details.email,
          name: data.paymentMethod?.billing_details.name,
          invoice_settings: {
            default_payment_method: data.paymentMethod?.id,
          },
          metadata: { firebaseUID: context.auth.uid },
        });

        await db
          .collection("users")
          .doc(context.auth.uid)
          .update({ stripeId: customer.id });
      }

      const subscription = await stripe.subscriptions.create({
        customer: customer?.id || userData?.stripeId,
        items: [{ plan: config.env.stripe.plan }],
        expand: ["latest_invoice.payment_intent"],
      });

      return {
        data: {
          subscription,
        },
        error: null,
      };
    } catch (error) {
      console.log(error);
      return {
        data: null,
        error,
      };
    }
  });

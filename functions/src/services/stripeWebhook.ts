import * as functions from "firebase-functions";
import mailgun from "mailgun-js";
import { config } from "../config";
import { auth, db, mg, stripe } from "../utils/init";

export const stripeWebhook = functions.region("europe-west1").https.onRequest(async (req, res) => {
  let event;
  try {
    event = stripe.webhooks.constructEvent(
      req.rawBody,
      req.headers["stripe-signature"]!,
      config.env.stripe.webhook_secret
    );
    res.status(200).send();
  } catch (err) {
    return res.status(400).send(`Webhook Error: ${err.message}`);
  }

  switch (event.type) {
    case "customer.subscription.created":
      try {
        const { customer } = event.data.object;
        const snapshots = await db
          .collection("users")
          .where("stripeId", "==", customer)
          .get();

        const userData = await auth.getUser(snapshots.docs[0].id);
        const emailData:
          | mailgun.messages.SendData
          | mailgun.messages.BatchData
          | mailgun.messages.SendTemplateData = {
          from: "Revealater <hello@revealater.com>",
          to: userData.email!,
          subject: "Your subscription at Revealater has been created successfuly!",
          text: `Thank you for your purchase, ${userData.displayName}. You can start using the Revealater pro features now!`,
        };

        await mg.messages().send(emailData);
        await auth.setCustomUserClaims(userData.uid, {
          ...userData.customClaims,
          isSubscribed: true,
        });
        break;
      } catch (error) {
        console.log(error);
        break;
      }
    case "invoice.payment_succeeded":
      try {
        const { customer_email } = event.data.object;
        const user = await auth.getUserByEmail(customer_email);

        const now = new Date();
        await auth.setCustomUserClaims(user.uid, {
          ...user.customClaims,
          proUntil: now.setMonth(now.getMonth() + 1),
        });
        break;
      } catch (error) {
        console.log(error);
        break;
      }
    default:
      return res.status(400).end();
  }
});

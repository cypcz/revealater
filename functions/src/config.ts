import * as functions from "firebase-functions";
import fs from "fs";
import path from "path";

const config = functions.config();

if (process.env.NODE_ENV !== "production") {
  const configPath = path.resolve(__dirname, "./.env.json");
  if (fs.existsSync(configPath)) {
    const env = require(configPath);
    config.env = env;
  }
}

export { config };

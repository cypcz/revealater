import * as admin from "firebase-admin";
import mailgun from "mailgun-js";
import Stripe from "stripe";
import { config } from "../config";

admin.initializeApp({
  credential: admin.credential.cert(
    JSON.parse(Buffer.from(config.env.firebase_account, "base64").toString())
  ),
});
const db = admin.firestore();
const auth = admin.auth();
const mg = mailgun({
  apiKey: config.env.mg.key,
  domain: config.env.mg.domain,
  host: "api.eu.mailgun.net",
});
const stripe = new Stripe(config.env.stripe.secret_key, { apiVersion: "2020-03-02" });

export { auth, db, mg, stripe };

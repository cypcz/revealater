module.exports = {
  siteMetadata: {
    title: `Revealater`,
    description: `Encrypt content and make it available anytime to anyone`,
    author: `@cyp`,
    siteUrl: "localhost:8000",
  },
  plugins: [
    `gatsby-plugin-typescript`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/static/assets`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: `${__dirname}/src/static/assets`,
        },
      },
    },
    {
      resolve: `gatsby-plugin-material-ui`,
      options: {
        stylesProvider: {
          injectFirst: true,
        },
      },
    },
    `gatsby-plugin-emotion`,
    {
      resolve: `gatsby-plugin-alias-imports`,
      options: {
        alias: {
          "@typings": "src/typings",
          "@components": "src/components",
          "@containers": "src/containers",
          "@static": "src/static",
          "@pages": "src/pages",
          "@routes": "src/routes",
          "@context": "src/context",
          "@utils": "src/utils",
          "@stylesheet": "src/stylesheet",
        },
        extensions: ["ts", "tsx"],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Revealater`,
        short_name: `Revealater`,
        start_url: `/`,
        icon: `src/static/assets/favicon.png`,
      },
    },
    {
      resolve: "gatsby-plugin-html-attributes",
      options: {
        lang: "en",
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
};

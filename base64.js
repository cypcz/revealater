// From JS object to base64 string
console.log(Buffer.from(JSON.stringify({})).toString("base64"));

// From base64 string to JS object
console.log(JSON.parse(Buffer.from("", "base64").toString()));

export enum Routes {
  HOME = "/",
  APP = "/app",
  REVEAL = "/app/reveal/:id",
  PLANS = "/app/plans",
  SCHEDULED = "/app/scheduled",
  SETTINGS = "/app/settings",
  CHECKOUT = "/app/checkout",
  PRIVACY = "/privacy",
  TERMS = "/terms",
}

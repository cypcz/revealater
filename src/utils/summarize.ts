export default (str: string, max: number) => {
  if (str.length <= max) return str;
  const subString = str.substring(0, max - 1);
  return subString.substring(0, subString.lastIndexOf(" ")) + "\u2026";
};

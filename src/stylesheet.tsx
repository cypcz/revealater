import styled, { CreateStyled } from "@emotion/styled";
import { Theme } from "@material-ui/core";
import { createMuiTheme, responsiveFontSizes } from "@material-ui/core/styles";

const muiTheme = createMuiTheme({
  palette: {
    type: "light",
    primary: {
      main: "#1F2771",
    },
    text: {
      primary: "#47433D",
    },
  },
  typography: {
    fontFamily: ["Roboto", "sans-serif"].join(","),
    htmlFontSize: 10,
  },
});

export const theme = responsiveFontSizes(muiTheme);

export default styled as CreateStyled<Theme>;

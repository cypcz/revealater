import SubscriptionForm from "@components/SubscriptionForm";
import Unsubscribe from "@components/Unsubscribe";
import { Auth } from "@context/Authentication";
import { RouteComponentProps } from "@reach/router";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import React, { useContext } from "react";

const stripePromise = loadStripe(process.env.GATSBY_STRIPE_KEY!);

const Checkout = ({}: RouteComponentProps) => {
  const { user } = useContext(Auth);

  return user?.isSubscribed ? (
    <Unsubscribe />
  ) : (
    <Elements stripe={stripePromise}>
      <SubscriptionForm user={user} />
    </Elements>
  );
};

export default Checkout;

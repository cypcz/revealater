import Cards from "@components/LandingPage/Plans/cards";
import { Auth } from "@context/Authentication";
import { RouteComponentProps } from "@reach/router";
import "firebase/storage";
import React, { useContext } from "react";

export default ({}: RouteComponentProps) => {
  const { user } = useContext(Auth);

  return <Cards user={user} />;
};

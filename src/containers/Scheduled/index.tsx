import Progress from "@components/Progress";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Typography from "@material-ui/core/Typography";
import GetAppIcon from "@material-ui/icons/GetApp";
import { RouteComponentProps } from "@reach/router";
import summarize from "@utils/summarize";
import { format } from "date-fns";
import firebase from "firebase/app";
import "firebase/functions";
import React, { useEffect, useState } from "react";
import { DownloadLink } from "./styled";

const DEV = process.env.NODE_ENV === "development";

interface Timestamp {
  _seconds: number;
  _nanoseconds: number;
}

interface Data {
  id: string;
  fileUrl: string | null;
  notifyOn: Timestamp[];
  notifiedOn: Timestamp[];
  subject?: string;
  message: string;
  to: string;
}

export default ({}: RouteComponentProps) => {
  const [data, setData] = useState<Data[] | null>(null);

  useEffect(() => {
    if (DEV) {
      firebase.functions().useFunctionsEmulator("http://localhost:5000");
    }
    firebase
      .app()
      .functions(DEV ? undefined : "europe-west1")
      .httpsCallable("getMyTasks")()
      .then(({ data }) => {
        setData(
          data.data.sort((a: any, b: any) => b.notifiedOn[0]?._seconds - a.notifiedOn[0]?._seconds)
        );
      });
  }, []);

  return (
    <TableContainer component={Paper}>
      {data ? (
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <Typography variant="body1">Scheduled for</Typography>
              </TableCell>
              <TableCell>
                <Typography variant="body1">Subject</Typography>
              </TableCell>
              <TableCell>
                <Typography variant="body1">Message</Typography>
              </TableCell>
              <TableCell>
                <Typography variant="body1">Notified on</Typography>
              </TableCell>
              <TableCell>
                <Typography variant="body1">To be notified on</Typography>
              </TableCell>
              <TableCell>
                <Typography variant="body1">File</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map(row => (
              <TableRow key={row.id}>
                <TableCell component="th" scope="row">
                  {row.to}
                </TableCell>
                <TableCell>{summarize(row.subject || "", 30)}</TableCell>
                <TableCell>{summarize(row.message, 50)}</TableCell>
                <TableCell>
                  {row.notifiedOn.map((date, index) => (
                    <div key={index}>
                      {format(new Date(date._seconds * 1000), "dd.MM.yyyy HH:mm")}
                      <br />
                    </div>
                  ))}
                </TableCell>
                <TableCell>
                  {row.notifyOn.map((date, index) => (
                    <div key={index}>
                      {format(new Date(date._seconds * 1000), "dd.MM.yyyy HH:mm")}
                      <br />
                    </div>
                  ))}
                </TableCell>
                <TableCell>
                  {row.fileUrl && (
                    <DownloadLink href={row.fileUrl} target="_blank">
                      <GetAppIcon />
                    </DownloadLink>
                  )}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      ) : (
        <Progress />
      )}
    </TableContainer>
  );
};

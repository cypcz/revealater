import styled from "@stylesheet";

export const DownloadLink = styled.a`
  color: ${({ theme }) => theme.palette.text.primary};
`;

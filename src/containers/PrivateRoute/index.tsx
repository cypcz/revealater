import PrivateLayout from "@containers/PrivateLayout";
import { Auth } from "@context/Authentication";
import { RouteComponentProps } from "@reach/router";
import { Routes } from "@utils/routes";
import { navigate } from "gatsby";
import React, { useContext } from "react";

interface Props extends RouteComponentProps {
  component: React.ElementType;
}

const PrivateRoute = ({ component: Component }: Props) => {
  const { user } = useContext(Auth);

  if (!user) {
    navigate(Routes.APP);
  }

  return (
    <PrivateLayout>
      <Component />
    </PrivateLayout>
  );
};

export default PrivateRoute;

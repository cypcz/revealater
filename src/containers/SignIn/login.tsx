import TextInput from "@components/FormInputs/TextInput";
import { Auth } from "@context/Authentication";
import Typography from "@material-ui/core/Typography";
import { Routes } from "@utils/routes";
import { Form, Formik } from "formik";
import React, { useContext } from "react";
import { SpanToggle, StyledButton, StyledLink } from "./styled";

interface FormValues {
  email: string;
  password: string;
}

interface Props {
  showSignup: boolean;
  onToggleSignup: (showSignup: boolean) => void;
}

export default ({ showSignup, onToggleSignup }: Props) => {
  const { signIn } = useContext(Auth);

  return (
    <>
      <Formik<FormValues>
        initialValues={{ email: "", password: "" }}
        validationSchema={null}
        onSubmit={values => signIn(values.email, values.password)}
      >
        {() => (
          <Form>
            <TextInput id="email" name="email" margin="normal" fullWidth label="Email address" />
            <TextInput
              id="password"
              name="password"
              margin="normal"
              fullWidth
              label="Password"
              type="password"
            />
            <Typography variant="subtitle2">
              By logging in, you agree to our{" "}
              <StyledLink to={Routes.TERMS}>terms of service</StyledLink> and{" "}
              <StyledLink to={Routes.PRIVACY}>privacy policy</StyledLink>.
            </Typography>
            <StyledButton type="submit" variant="contained" fullWidth color="primary">
              Log in
            </StyledButton>
          </Form>
        )}
      </Formik>
      <Typography variant="body2" style={{ marginTop: "2px" }}>
        Looking to{" "}
        <SpanToggle onClick={() => onToggleSignup(!showSignup)}>create an account?</SpanToggle>
      </Typography>
    </>
  );
};

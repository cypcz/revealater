import { Auth } from "@context/Authentication";
import { Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { RouteComponentProps } from "@reach/router";
import { Routes } from "@utils/routes";
import { navigate } from "gatsby";
import React, { useContext, useState } from "react";
import Background from "./background";
import LoginContent from "./login";
import RegisterContent from "./register";
import {
  ButtonGroup,
  FacebookButton,
  FacebookIcon,
  FacebookText,
  GoogleButton,
  GoogleIcon,
  GoogleText,
  LogoIcon,
  StyledDivider,
  StyledLeftSide,
  Wrapper,
} from "./styled";

interface LocationProps {
  location: { state: { signup: boolean } };
}

export default ({ location }: RouteComponentProps<LocationProps>) => {
  const { googleLogin, facebookLogin } = useContext(Auth);
  const [showSignup, toggleSignup] = useState(!!location?.state?.signup);

  return (
    <Wrapper>
      <StyledLeftSide>
        <LogoIcon onClick={() => navigate(Routes.HOME)} />
        <ButtonGroup>
          <GoogleButton onClick={() => googleLogin()}>
            <GoogleIcon />
            <GoogleText>Sign in with Google</GoogleText>
          </GoogleButton>
          <FacebookButton onClick={() => facebookLogin()}>
            <FacebookIcon />
            <FacebookText>Sign in with Facebook</FacebookText>
          </FacebookButton>
        </ButtonGroup>
        <Grid container justify="center" alignItems="center" style={{ margin: "2rem 0" }}>
          <StyledDivider variant="middle" />
          <Typography variant="body1">or</Typography>
          <StyledDivider variant="middle" />
        </Grid>
        {showSignup ? (
          <RegisterContent showSignup={showSignup} onToggleSignup={toggleSignup} />
        ) : (
          <LoginContent showSignup={showSignup} onToggleSignup={toggleSignup} />
        )}
      </StyledLeftSide>
      <Background />
    </Wrapper>
  );
};

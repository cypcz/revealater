import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import FbIcon from "@static/assets/facebook.svg";
import GIcon from "@static/assets/google.svg";
import Logo from "@static/assets/logo.svg";
import styled from "@stylesheet";
import { Link } from "gatsby";
import BackgroundImage from "gatsby-background-image";

export const FacebookIcon = styled(FbIcon)`
  fill: ${({ theme }) => theme.palette.common.white};
  width: 25px;
  height: 25px;
  margin-right: 10px;
`;

export const GoogleIcon = styled(GIcon)`
  width: 25px;
  margin-right: 10px;
`;

export const StyledDivider = styled(Divider)`
  flex-grow: 1;
`;

export const SpanToggle = styled.span`
  color: ${({ theme }) => theme.palette.primary.main};
  :hover {
    cursor: pointer;
  }
`;

export const Wrapper = styled.div`
  min-height: 100vh;
  display: flex;
`;

export const StyledLeftSide = styled.div`
  flex: 3;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: ${({ theme }) => theme.palette.grey[200]};
  padding: 20px;
`;

export const LogoIcon = styled(Logo)`
  margin: 0 auto 30px auto;

  :hover {
    cursor: pointer;
  }
`;

export const GoogleText = styled(Typography)`
  font-size: 1.4rem;
  color: ${({ theme }) => theme.palette.text.primary};
`;

export const FacebookText = styled(Typography)`
  font-size: 1.4rem;
  color: ${({ theme }) => theme.palette.common.white};
`;

export const StyledButton = styled(Button)`
  margin-top: 20px;
`;

export const GoogleButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${({ theme }) => theme.palette.common.white};
  padding: 0;
  height: 50px;
  width: 200px;
  border: none;
  padding: 10px;
  border: ${({ theme }) => `1px solid ${theme.palette.grey[500]}`};
  border-radius: 3px;
  transition: 300ms cubic-bezier(0.08, 0.52, 0.52, 1) background-color;

  ${({ theme }) => `
    ${theme.breakpoints.down("xs")} {
      margin-bottom: 10px;
    }
  `}
    :hover {
    cursor: pointer;
    background: #ebedf0;
  }

  :focus {
    outline: 0;
  }
`;

export const FacebookButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  background: #4267b2;
  padding: 0;
  width: 200px;
  height: 50px;
  color: ${({ theme }) => theme.palette.common.white};
  border: none;
  padding: 10px;
  border-radius: 3px;
  transition: 300ms cubic-bezier(0.08, 0.52, 0.52, 1) background-color;

  :hover {
    cursor: pointer;
    background: #365899;
  }

  :focus {
    outline: 0;
  }
`;

export const ButtonGroup = styled.div`
  display: flex;
  justify-content: space-evenly;

  ${({ theme }) => `
    ${theme.breakpoints.down("xs")} {
      align-items: center;
      flex-direction: column;
    }
  `}
`;

export const StyledBgImage = styled(BackgroundImage)`
  flex: 2;

  ${({ theme }) => `
    ${theme.breakpoints.down("sm")} {
      display: none;
    }
  `}
`;

export const StyledLink = styled(Link)`
  color: ${({ theme }) => theme.palette.primary.main};
  text-decoration: none;

  :visited {
    color: ${({ theme }) => theme.palette.primary.main};
  }
`;

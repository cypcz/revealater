import { graphql, StaticQuery } from "gatsby";
import React from "react";
import { StyledBgImage } from "./styled";

export default () => (
  <StaticQuery
    query={graphql`
      query {
        bottle: file(relativePath: { eq: "bottle.jpg" }) {
          childImageSharp {
            fluid(quality: 90) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
    `}
    render={data => <StyledBgImage Tag="div" fluid={data.bottle.childImageSharp.fluid} />}
  />
);

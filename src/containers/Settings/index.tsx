import TextInput from "@components/FormInputs/TextInput";
import UploadInput from "@components/FormInputs/UploadInput";
import { Auth } from "@context/Authentication";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { RouteComponentProps } from "@reach/router";
import firebase from "firebase/app";
import "firebase/storage";
import { Formik } from "formik";
import { useSnackbar } from "notistack";
import React, { useContext, useState } from "react";
import * as Yup from "yup";
import { StyledForm, UploadSaveContainer, Wrapper } from "./styled";

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .email("Please enter valid email address")
    .required("This field is required"),
  name: Yup.string().required("This field is required"),
  newImage: Yup.mixed().test("newImage", "File cannot be larger than 2MB", value => {
    if (value) return value.size <= 2097152;
    else return true;
  }),
});

interface FormValues {
  email: string;
  name: string;
  image: any;
  imagePreview: string;
}

export default ({}: RouteComponentProps) => {
  const { user } = useContext(Auth);
  const [submitting, setSubmitting] = useState(false);
  const { enqueueSnackbar } = useSnackbar();

  const handleSubmit = async (values: FormValues) => {
    try {
      setSubmitting(true);
      if (values?.image.size) {
        await firebase
          .storage()
          .ref()
          .child(`profileImages/${Date.now()}_${values.image.name}`)
          .put(values.image)
          .then(
            async snapshot =>
              await user?.updateProfile({
                displayName: values.name,
                photoURL: await snapshot.ref.getDownloadURL(),
              })
          );
      } else {
        await user?.updateProfile({ displayName: values.name });
      }

      setSubmitting(false);
      enqueueSnackbar(<Typography>Your profile was successfuly updated</Typography>, {
        autoHideDuration: 3000,
        variant: "success",
      });
    } catch (error) {
      console.log(error);
      setSubmitting(false);
      enqueueSnackbar(<Typography>{error.message}</Typography>, {
        autoHideDuration: 3000,
        variant: "error",
      });
    }
  };

  return (
    <Wrapper>
      <Formik<FormValues>
        initialValues={{
          email: user?.email!,
          name: user?.displayName!,
          image: user?.photoURL,
          imagePreview: "",
        }}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ values }) => (
          <StyledForm>
            <TextInput
              id="email"
              label="Email"
              name="email"
              margin="normal"
              size="small"
              disabled
              fullWidth
            />
            <TextInput id="name" label="Name" name="name" margin="normal" size="small" fullWidth />
            <UploadSaveContainer>
              <UploadInput
                name="image"
                image={values.imagePreview ? values.imagePreview : user?.photoURL}
                progress={2}
              />
              <Button
                type="submit"
                variant="contained"
                color="primary"
                disabled={submitting}
                style={{ width: "100px" }}
              >
                Save
              </Button>
            </UploadSaveContainer>
          </StyledForm>
        )}
      </Formik>
    </Wrapper>
  );
};

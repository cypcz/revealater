import styled from "@stylesheet";
import { Form } from "formik";

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;

export const StyledForm = styled(Form)`
  max-width: 600px;
  width: 100%;
`;

export const UploadSaveContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  margin-top: 30px;
`;

import MainForm from "@components/MainForm";
import { Auth } from "@context/Authentication";
import { RouteComponentProps } from "@reach/router";
import React, { useContext } from "react";

const Dashboard = ({}: RouteComponentProps) => {
  const { user } = useContext(Auth);
  return <MainForm user={user} />;
};

export default Dashboard;

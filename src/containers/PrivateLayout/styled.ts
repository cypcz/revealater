import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Logo from "@static/assets/logo.svg";
import styled from "@stylesheet";

interface Scrolled {
  isScrolled: boolean;
}

export const StyledContainer = styled(Container)`
  padding-top: 130px;
  min-height: 100vh;
`;

export const Wrapper = styled.div`
  background-color: ${({ theme }) => theme.palette.grey[200]};
`;

export const HeaderWrap = styled.header<Scrolled>`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 80px;
  width: 100%;
  padding: 0 60px;
  position: fixed;
  top: 0;
  background-color: ${({ theme }) => theme.palette.grey[200]};
  box-shadow: ${({ isScrolled, theme }) =>
    isScrolled ? `0px 1px 5px 0px ${theme.palette.text.primary}` : ""};
  transition: 0.3s all;
  z-index: 1000;

  ${({ theme }) => `
    ${theme.breakpoints.down("xs")} {
      justify-content: center;
    }
  `}
`;

export const Profile = styled.img`
  border-radius: 50%;
  width: 50px;
  height: 50px;
  :hover {
    cursor: pointer;
  }
`;

export const LogoPart = styled.div`
  display: flex;
  :hover {
    cursor: pointer;
  }
`;

export const LogoIcon = styled(Logo)`
  margin-right: 20px;

  ${({ theme }) => `
    ${theme.breakpoints.down("xs")} {
      margin: 0;
    }
  `}
`;

export const LogoHeading = styled.p`
  font-family: Sriracha;
  font-size: 2rem;
  margin: auto 0;
  text-transform: uppercase;
  color: ${({ theme }) => theme.palette.text.primary};

  ${({ theme }) => `
    ${theme.breakpoints.down("xs")} {
      display: none;
    }
  `}
`;

export const NavigationPart = styled.nav`
  display: flex;
`;

export const NavButton = styled(Button)`
  font-family: Simplifica;
  font-size: 2rem;
  letter-spacing: 1px;
  font-weight: bold;
  text-transform: lowercase;
  margin-right: 20px;
  :last-child {
    margin-right: 0;
  }
`;

export const PopoverContent = styled.div`
  width: 200px;
  padding: 10px 15px;
  display: flex;
  flex-direction: column;
  background-color: ${({ theme }) => theme.palette.grey[100]};
`;

export const Name = styled(Typography)`
  font-size: 1.6rem;
  font-weight: ${({ theme }) => theme.typography.fontWeightBold};
  letter-spacing: 1px;
  margin-top: 5px;
`;

export const Email = styled(Typography)`
  font-size: 1.2rem;
  color: ${({ theme }) => theme.palette.grey[600]};
  margin-top: 5px;
`;

export const Span = styled.span`
  margin-top: 5px;
  margin-bottom: 30px;
  color: ${({ theme }) => theme.palette.text.primary};
`;

export const SpanHighlighted = styled.span`
  color: ${({ theme }) => theme.palette.primary.main};
  :hover {
    cursor: pointer;
  }
`;

export const CustomButton = styled.button`
  background: transparent;
  color: ${({ theme }) => theme.palette.text.primary};
  font-family: ${({ theme }) => theme.typography.fontFamily};
  font-size: 1.6rem;
  text-align: left;
  outline: none;
  border: none;
  padding: 0;

  :first-of-type {
    margin-bottom: 10px;
  }

  :hover {
    cursor: pointer;
  }
`;

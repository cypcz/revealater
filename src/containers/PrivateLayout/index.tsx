import { Auth } from "@context/Authentication";
import Popover from "@material-ui/core/Popover";
import { Routes } from "@utils/routes";
import { navigate } from "gatsby";
import React, { useContext, useEffect, useState } from "react";
import {
  CustomButton,
  Email,
  HeaderWrap,
  LogoHeading,
  LogoIcon,
  LogoPart,
  Name,
  NavButton,
  NavigationPart,
  PopoverContent,
  Profile,
  Span,
  SpanHighlighted,
  StyledContainer,
  Wrapper,
} from "./styled";

interface Props {
  children: JSX.Element | JSX.Element[];
}

const PrivateLayout = ({ children }: Props) => {
  const [scrolled, toggleScrolled] = useState(false);
  const [anchorEl, setAnchorEl] = React.useState<HTMLImageElement | null>(null);
  const { user, signOut } = useContext(Auth);

  const listenScrollEvent = () => {
    if (window.scrollY > 20) {
      toggleScrolled(true);
    } else {
      toggleScrolled(false);
    }
  };

  const handleClick = (event: React.MouseEvent<HTMLImageElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    window.addEventListener("scroll", listenScrollEvent);

    return () => {
      window.removeEventListener("scroll", listenScrollEvent);
    };
  }, []);

  return (
    <Wrapper>
      <HeaderWrap isScrolled={scrolled}>
        <LogoPart onClick={() => navigate(Routes.APP)}>
          <LogoIcon />
          <LogoHeading>Revealater</LogoHeading>
        </LogoPart>
        <NavigationPart>
          <NavButton onClick={() => navigate(Routes.PLANS)}>Plans</NavButton>
          <NavButton onClick={() => navigate(Routes.SCHEDULED)}>Scheduled</NavButton>
          <NavButton onClick={() => navigate(Routes.CHECKOUT)}>Checkout</NavButton>
          <Profile src={user?.photoURL!} onClick={handleClick} alt="profile" />
          <Popover
            open={Boolean(anchorEl)}
            onClose={handleClose}
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "left",
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "center",
            }}
          >
            <PopoverContent>
              <Name>{user?.displayName}</Name>
              <Email>{user?.email}</Email>
              <Span>
                on{" "}
                <SpanHighlighted onClick={() => navigate(Routes.PLANS)}>
                  {user?.isPro ? "pro" : "basic"}
                </SpanHighlighted>{" "}
                plan
              </Span>
              <CustomButton onClick={() => navigate(Routes.SETTINGS)}>Settings</CustomButton>
              <CustomButton onClick={() => signOut()}>Sign out</CustomButton>
            </PopoverContent>
          </Popover>
        </NavigationPart>
      </HeaderWrap>
      <StyledContainer maxWidth="lg">{children}</StyledContainer>
    </Wrapper>
  );
};

export default PrivateLayout;

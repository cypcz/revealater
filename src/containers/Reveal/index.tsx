import TextInput from "@components/FormInputs/TextInput";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import GetAppIcon from "@material-ui/icons/GetApp";
import { RouteComponentProps } from "@reach/router";
import { Routes } from "@utils/routes";
import format from "date-fns/format";
import toDate from "date-fns/toDate";
import firebase from "firebase/app";
import "firebase/functions";
import { Form, Formik } from "formik";
import { navigate } from "gatsby";
import React, { useState } from "react";
import * as Yup from "yup";
import {
  ContentHeading,
  DownloadLink,
  InlineInfo,
  LogoHeading,
  LogoIcon,
  LogoPart,
  NotReadyIcon,
  Wrapper,
} from "./styled";

const validationSchema = Yup.object().shape({
  password: Yup.string().required("This field is required"),
});

interface FormValues {
  password: string;
}

interface State {
  data: firebase.firestore.DocumentData | null;
  error: string | null;
}

interface Props extends RouteComponentProps {
  id?: string;
}

const DEV = process.env.NODE_ENV === "development";

const Reveal = ({ id }: Props) => {
  const [state, setState] = useState<State>({ data: null, error: null });
  const [loading, toggleLoading] = useState(false);

  const handleSubmit = async ({ password }: FormValues) => {
    if (DEV) {
      firebase.functions().useFunctionsEmulator("http://localhost:5000");
    }
    toggleLoading(true);
    const { data } = await firebase
      .app()
      .functions(DEV ? undefined : "europe-west1")
      .httpsCallable("getContent")({ id, password });

    setState({ data: data.data, error: data.error });
    toggleLoading(false);
  };

  return (
    <Wrapper>
      <LogoPart onClick={() => navigate(Routes.HOME)}>
        <LogoIcon />
        <LogoHeading>Revealater</LogoHeading>
      </LogoPart>
      {!state.data ? (
        <>
          <Typography variant="body2" style={{ marginBottom: "2rem" }}>
            Enter the password you received in your email to access to your content.
          </Typography>
          <Formik<FormValues>
            initialValues={{
              password: "",
            }}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            {({}) => (
              <Form style={{ width: "100%" }}>
                <TextInput
                  id="password"
                  label="Password"
                  name="password"
                  placeholder="Password"
                  margin="normal"
                  type="password"
                  fullWidth
                />
                <Button
                  type="submit"
                  variant="contained"
                  fullWidth
                  color="primary"
                  disabled={loading}
                  style={{ margin: "20px 0 10px 0" }}
                >
                  Get content
                </Button>
              </Form>
            )}
          </Formik>
          <Typography variant="body2" color="error">
            {state.error && "The entered password doesn't belong to any scheduled content."}
          </Typography>
        </>
      ) : (
        <>
          {state.data.message ? (
            <>
              <InlineInfo>
                <ContentHeading variant="body1">Message to:</ContentHeading>
                <Typography variant="body2"> {state.data.to}</Typography>
              </InlineInfo>
              <InlineInfo>
                <ContentHeading variant="body1">Message from:</ContentHeading>
                <Typography variant="body2">{state.data.from}</Typography>
              </InlineInfo>
              <InlineInfo>
                <ContentHeading variant="body1">Subject:</ContentHeading>
                <Typography variant="body2">{state.data.subject}</Typography>
              </InlineInfo>
              <ContentHeading variant="body1">Message:</ContentHeading>
              <Typography variant="body2">{state.data.message}</Typography>
              {state.data.fileUrl && (
                <InlineInfo>
                  <ContentHeading variant="body1">Attachments:</ContentHeading>
                  <DownloadLink href={state.data.fileUrl} target="_blank" download>
                    <GetAppIcon />
                  </DownloadLink>
                </InlineInfo>
              )}
            </>
          ) : (
            <>
              <Typography variant="body2" style={{ marginBottom: "1rem" }}>
                Unfortunately, your content is not ready to be revealed yet. It becomes available on
              </Typography>
              <ContentHeading variant="body1">
                {format(toDate(state.data.notifyOn._seconds * 1000), "dd.MM.yyyy HH:mm")}
              </ContentHeading>
              <NotReadyIcon />
            </>
          )}
        </>
      )}
    </Wrapper>
  );
};
export default Reveal;

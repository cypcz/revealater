import Typography from "@material-ui/core/Typography";
import Logo from "@static/assets/logo.svg";
import NotReady from "@static/assets/not_ready.svg";
import styled from "@stylesheet";

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 3rem auto;
  width: 100%;
  max-width: 600px;
`;

export const LogoPart = styled.div`
  margin: 0 auto 2rem auto;
  flex-direction: column;
  align-items: center;
  display: flex;
  :hover {
    cursor: pointer;
  }
`;

export const LogoIcon = styled(Logo)``;
export const NotReadyIcon = styled(NotReady)`
  max-width: 300px;
  max-height: 300px;
`;

export const LogoHeading = styled.p`
  font-family: Sriracha;
  font-size: 2rem;
  text-transform: uppercase;
  color: ${({ theme }) => theme.palette.text.primary};
  margin-bottom: 3rem;
`;

export const InlineInfo = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 2rem;

  :last-of-type {
    margin-top: 2rem;
  }
`;

export const ContentHeading = styled(Typography)`
  font-weight: bold;
  margin-right: 5px;
`;

export const DownloadLink = styled.a`
  color: ${({ theme }) => theme.palette.text.primary};
`;

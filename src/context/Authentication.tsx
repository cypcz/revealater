import Typography from "@material-ui/core/Typography";
import firebase from "firebase/app";
import "firebase/auth";
import { useSnackbar } from "notistack";
import React, { createContext, useEffect, useState } from "react";

export interface User extends firebase.User {
  isPro: boolean;
  isSubscribed: boolean;
}

interface ContextProps {
  user: User | null;
  loading: boolean;
  signOut: () => void;
  signIn: (email: string, password: string) => void;
  register: (email: string, password: string, name: string) => Promise<void>;
  googleLogin: () => void;
  facebookLogin: () => void;
}

interface State {
  loading: boolean;
  user: User | null;
}

interface Props {
  children: JSX.Element | JSX.Element[];
}

export const AuthenticationProvider = ({ children }: Props) => {
  const { enqueueSnackbar } = useSnackbar();
  const [state, setState] = useState<State>({ loading: true, user: null });

  useEffect(() => {
    firebase.auth().onAuthStateChanged(async user => {
      if (user) {
        await user.getIdToken(true);
        const { claims } = await user.getIdTokenResult();
        const isPro = claims.proUntil ? claims.proUntil >= new Date().getTime() : false;
        setState({
          user: Object.assign(user, { isPro, isSubscribed: claims.isSubscribed }),
          loading: false,
        });
      } else {
        setState({ ...state, loading: false });
      }
    });
  }, []);

  const register = async (email: string, password: string, name: string) => {
    return new Promise<void>(async (resolve, reject) => {
      try {
        const credential = await firebase.auth().createUserWithEmailAndPassword(email, password);
        if (credential.user) {
          await Promise.all([
            credential.user.updateProfile({
              displayName: name,
              photoURL:
                "https://firebasestorage.googleapis.com/v0/b/tidy-etching-248809.appspot.com/o/profileImages%2Fuser.svg?alt=media&token=1eb87b94-7606-4532-8239-a078e37bae56",
            }),
            //  credential.user.sendEmailVerification(),
          ]);
        }
        enqueueSnackbar(<Typography>Registration successful</Typography>, {
          autoHideDuration: 3000,
          variant: "success",
        });
        resolve();
      } catch (error) {
        console.log(error);
        enqueueSnackbar(<Typography>{error.message}</Typography>, {
          autoHideDuration: 3000,
          variant: "error",
        });
        reject();
      }
    });
  };

  const googleLogin = async () => {
    try {
      const googleProvider = new firebase.auth.GoogleAuthProvider();
      await firebase.auth().signInWithPopup(googleProvider);
    } catch (error) {
      console.log(error);
      enqueueSnackbar(<Typography>{error.message}</Typography>, {
        autoHideDuration: 3000,
        variant: "error",
      });
    }
  };

  const facebookLogin = async () => {
    try {
      const facebookProvider = new firebase.auth.FacebookAuthProvider();
      await firebase.auth().signInWithPopup(facebookProvider);
    } catch (error) {
      console.log(error);
      enqueueSnackbar(<Typography>{error.message}</Typography>, {
        autoHideDuration: 3000,
        variant: "error",
      });
    }
  };

  const signIn = async (email: string, password: string) => {
    try {
      await firebase.auth().signInWithEmailAndPassword(email, password);
    } catch (error) {
      console.log(error);
      enqueueSnackbar(<Typography>{error.message}</Typography>, {
        autoHideDuration: 3000,
        variant: "error",
      });
    }
  };

  const signOut = async () => {
    try {
      await firebase.auth().signOut();
    } catch (error) {
      console.log(error);
      enqueueSnackbar(<Typography>{error.message}</Typography>, {
        autoHideDuration: 3000,
        variant: "error",
      });
    }
  };

  return (
    <Auth.Provider
      value={{
        user: state.user,
        loading: state.loading,
        signIn,
        register,
        googleLogin,
        facebookLogin,
        signOut,
      }}
    >
      {children}
    </Auth.Provider>
  );
};

export const Auth = createContext({ user: null, loading: true } as ContextProps);

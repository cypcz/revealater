import LandingPage from "@components/LandingPage";
import SEO from "@components/SEO";
import React from "react";

export default () => (
  <>
    <SEO title="Revealater" description="Schedule anything for anyone" />
    <LandingPage />
  </>
);

import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import styled from "@stylesheet";
import { Routes } from "@utils/routes";
import { Link } from "gatsby";
import React from "react";

export default () => (
  <Wrapper>
    <Container maxWidth="lg">
      <Typography variant="h3" component="h1">
        Terms of Service
      </Typography>
      <br />
      <Typography variant="body1">
        Please read these terms of service ("terms", "terms of service") carefully before using
        Revealater website (the "service") operated by Jakub Kot ("us", 'we", "our").
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        Conditions of Use
      </Typography>
      <br />
      <Typography variant="body1">
        We will provide their services to you, which are subject to the conditions stated below in
        this document. Every time you visit this website, use its services or make a purchase, you
        accept the following conditions. This is why we urge you to read them carefully.
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        Privacy Policy
      </Typography>
      <br />
      <Typography variant="body1">
        Before you continue using our website we advise you to read our{" "}
        <Link to={Routes.PRIVACY}>privacy policy</Link> regarding our user data collection. It will
        help you better understand our practices.
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        Copyright
      </Typography>
      <br />
      <Typography variant="body1">
        Content published on this website (digital downloads, images, texts, graphics, logos) is the
        property of Revealater and/or its content creators and protected by international copyright
        laws. The entire compilation of the content found on this website is the exclusive property
        of Revealater, with copyright authorship for this compilation by Revealater.
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        Communications
      </Typography>
      <br />
      <Typography variant="body1">
        The entire communication with us is electronic. Every time you send us an email or visit our
        website, you are going to be communicating with us. You hereby consent to receive
        communications from us. You also agree that all notices, disclosures, agreements and other
        communications we provide to you electronically meet the legal requirements that such
        communications be in writing.
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        Applicable Law
      </Typography>
      <br />
      <Typography variant="body1">
        By visiting this website, you agree that the laws of the Czech Republic, without regard to
        principles of conflict laws, will govern these terms of service, or any dispute of any sort
        that might come between Revealater and you, or its business partners and associates.
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        Disputes
      </Typography>
      <br />
      <Typography variant="body1">
        Any dispute related in any way to your visit to this website or to products you purchase
        from us shall be arbitrated by state or federal court Czech Republic and you consent to
        exclusive jurisdiction and venue of such courts.
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        Comments, Reviews, and Emails
      </Typography>
      <br />
      <Typography variant="body1">
        Visitors may post content as long as it is not obscene, illegal, defamatory, threatening,
        infringing of intellectual property rights, invasive of privacy or injurious in any other
        way to third parties. Content has to be free of software viruses, political campaign, and
        commercial solicitation.
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        License and Site Access
      </Typography>
      <br />
      <Typography variant="body1">
        We grant you a limited license to access and make personal use of this website. You are not
        allowed to download or modify it. This may be done only with written consent from us.
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        User Account
      </Typography>
      <br />
      <Typography variant="body1">
        If you are an owner of an account on this website, you are solely responsible for
        maintaining the confidentiality of your private user details (username and password). You
        are responsible for all activities that occur under your account or password.
      </Typography>
      <br />
      <Typography variant="body1">
        We reserve all rights to terminate accounts, edit or remove content and cancel orders in
        their sole discretion.
      </Typography>
    </Container>
  </Wrapper>
);

const Wrapper = styled.div`
  padding: 3rem 0;
  background-color: ${({ theme }) => theme.palette.grey[200]};
`;

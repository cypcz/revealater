import Progress from "@components/Progress";
import SEO from "@components/SEO";
import Checkout from "@containers/Checkout";
import Dashboard from "@containers/Dashboard";
import Plans from "@containers/Plans";
import PrivateRoute from "@containers/PrivateRoute";
import Reveal from "@containers/Reveal";
import Scheduled from "@containers/Scheduled";
import Settings from "@containers/Settings";
import SignIn from "@containers/SignIn";
import { Auth, AuthenticationProvider } from "@context/Authentication";
import { Router } from "@reach/router";
import "@stripe/stripe-js";
import { Routes } from "@utils/routes";
import firebase from "firebase/app";
import React, { useContext } from "react";

const AppPage = () => {
  const { user, loading } = useContext(Auth);

  if (typeof firebase === "undefined" || loading) {
    return <Progress />;
  }

  return (
    <Router>
      {user ? (
        <PrivateRoute path={Routes.APP} component={Dashboard} />
      ) : (
        <SignIn path={Routes.APP} />
      )}
      <PrivateRoute path={Routes.SETTINGS} component={Settings} />
      <PrivateRoute path={Routes.CHECKOUT} component={Checkout} />
      <PrivateRoute path={Routes.PLANS} component={Plans} />
      <PrivateRoute path={Routes.SCHEDULED} component={Scheduled} />
      <Reveal path={Routes.REVEAL} />
    </Router>
  );
};

const AppPageWrapper = () => (
  <AuthenticationProvider>
    <SEO title="Revealater" description="The App" />
    <AppPage />
  </AuthenticationProvider>
);

export default AppPageWrapper;

import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import styled from "@stylesheet";
import { Routes } from "@utils/routes";
import { Link } from "gatsby";
import React from "react";

export default () => (
  <Wrapper>
    <Container maxWidth="lg">
      <Typography variant="h3" component="h1">
        Privacy Policy of Revealater
      </Typography>
      <br />
      <Typography variant="body1">
        Revealater operates the https://revealater.com website, which provides the SERVICE.
      </Typography>
      <br />
      <Typography variant="body1">
        This page is used to inform website visitors regarding our policies with the collection,
        use, and disclosure of Personal Information if anyone decided to use our Service, the
        Revealater website.
      </Typography>
      <br />
      <Typography variant="body1">
        If you choose to use our Service, then you agree to the collection and use of information in
        relation with this policy. The Personal Information that we collect are used for providing
        and improving the Service. We will not use or share your information with anyone except as
        described in this Privacy Policy.
      </Typography>
      <br />
      <Typography variant="body1">
        The terms used in this Privacy Policy have the same meanings as in our{" "}
        <Link to={Routes.TERMS}>terms of service</Link>, unless otherwise defined in this Privacy
        Policy. Our Privacy Policy was created with the help of the{" "}
        <a target="__blank" rel="noopener" href="https://www.privacypolicytemplate.net">
          {" "}
          Privacy Policy Template{" "}
        </a>
        and the{" "}
        <a target="__blank" rel="noopener" href="https://www.privacy-policy-template.com/">
          Online Privacy Policy Template.
        </a>
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        Information Collection and Use
      </Typography>
      <br />
      <Typography variant="body1">
        For a better experience while using our Service, we may require you to provide us with
        certain personally identifiable information, including but not limited to your name or email
        address. The information that we collect will be used to contact or identify you.
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        Log Data
      </Typography>
      <br />
      <Typography variant="body1">
        We want to inform you that whenever you visit our Service, we collect information that your
        browser sends to us that is called Log Data. This Log Data may include information such as
        your computer’s Internet Protocol ("IP") address, browser version, pages of our Service that
        you visit, the time and date of your visit, the time spent on those pages, and other
        statistics.
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        Cookies
      </Typography>
      <br />
      <Typography variant="body1">
        Cookies are files with small amount of data that is commonly used an anonymous unique
        identifier. These are sent to your browser from the website that you visit and are stored on
        your computer’s hard drive.
      </Typography>
      <br />
      <Typography variant="body1">
        Our website uses these "cookies" to collection information and to improve our Service. You
        have the option to either accept or refuse these cookies, and know when a cookie is being
        sent to your computer. If you choose to refuse our cookies, you may not be able to use some
        portions of our Service.
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        Service Providers
      </Typography>
      <br />
      <Typography variant="body1">
        We may employ third-party companies and individuals due to the following reasons:
      </Typography>
      <br />
      <ul>
        <Typography variant="body1" component="li">
          To facilitate our Service;
        </Typography>
        <Typography variant="body1" component="li">
          To provide the Service on our behalf;
        </Typography>
        <Typography variant="body1" component="li">
          To perform Service-related services; or
        </Typography>
        <Typography variant="body1" component="li">
          To assist us in analyzing how our Service is used.
        </Typography>
      </ul>
      <br />
      <Typography variant="body1">
        We want to inform our Service users that these third parties have access to your Personal
        Information. The reason is to perform the tasks assigned to them on our behalf. However,
        they are obligated not to disclose or use the information for any other purpose.
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        Security
      </Typography>
      <br />
      <Typography variant="body1">
        We value your trust in providing us your Personal Information, thus we are striving to use
        commercially acceptable means of protecting it. But remember that no method of transmission
        over the internet, or method of electronic storage is 100% secure and reliable, and we
        cannot guarantee its absolute security.
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        Children’s Privacy
      </Typography>
      <br />
      <Typography variant="body1">
        Our Services do not address anyone under the age of 13. We do not knowingly collect personal
        identifiable information from children under 13. In the case we discover that a child under
        13 has provided us with personal information, we immediately delete this from our servers.
        If you are a parent or guardian and you are aware that your child has provided us with
        personal information, please contact us so that we will be able to do necessary actions.
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        Changes to This Privacy Policy
      </Typography>
      <br />
      <Typography variant="body1">
        We may update our Privacy Policy from time to time. Thus, we advise you to review this page
        periodically for any changes. We will notify you of any changes by posting the new Privacy
        Policy on this page. These changes are effective immediately, after they are posted on this
        page.
      </Typography>
      <br />
      <Typography variant="h4" component="h2">
        Contact Us
      </Typography>
      <br />
      <Typography variant="body1">
        If you have any questions or suggestions about our Privacy Policy, do not hesitate to
        contact us.
      </Typography>
    </Container>
  </Wrapper>
);

const Wrapper = styled.div`
  padding: 3rem 0;
  background-color: ${({ theme }) => theme.palette.grey[200]};
`;

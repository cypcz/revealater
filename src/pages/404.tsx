import { Routes } from "@utils/routes";
import { navigate } from "gatsby";
import { useEffect } from "react";

export default () => {
  useEffect(() => {
    navigate(Routes.HOME);
  }, []);
  return null;
};

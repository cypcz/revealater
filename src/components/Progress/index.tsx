import React from "react";
import { StyledProgress } from "./styled";

export default () => <StyledProgress />;

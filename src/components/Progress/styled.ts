import CircularProgress from "@material-ui/core/CircularProgress";
import styled from "@stylesheet";

export const StyledProgress = styled(CircularProgress)`
  margin: auto;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  position: fixed;
`;

import Features from "@components/LandingPage/Features";
import HowItWorks from "@components/LandingPage/HowItWorks";
import Plans from "@components/LandingPage/Plans";
import TopBar from "@components/LandingPage/TopBar";
import MainForm from "@components/MainForm";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import React from "react";
import { Link } from "react-scroll";
import Background from "./background";
import Footer from "./Footer";
import { MarginedGrid, StyledButton, StyledTypography, Wrapper } from "./styled";

export default () => (
  <>
    <Background>
      <Wrapper id="landing">
        <TopBar />
        <Container maxWidth="lg">
          <MarginedGrid container direction="row" justify="center" alignItems="center">
            <Grid container item md={6} direction="column" alignItems="center">
              <StyledTypography variant="h4" variantMapping={{ h4: "h2" }}>
                Surprise with content!
              </StyledTypography>
              <StyledTypography variant="h4" variantMapping={{ h4: "h1" }}>
                Schedule anything for anyone
              </StyledTypography>
              <Link to="howitworks" smooth offset={-60}>
                <StyledButton variant="contained" color="primary">
                  Explore more
                </StyledButton>
              </Link>
            </Grid>
            <Grid item md={6}>
              <MainForm user={null} />
            </Grid>
          </MarginedGrid>
        </Container>
      </Wrapper>
    </Background>
    <HowItWorks />
    <Features />
    <Plans />
    <Footer />
  </>
);

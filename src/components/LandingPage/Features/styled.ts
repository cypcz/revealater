import { css } from "@emotion/core";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import DocumentIcon from "@static/assets/document.svg";
import NotificationIcon from "@static/assets/notification.svg";
import SecurityIcon from "@static/assets/security.svg";
import TimeIcon from "@static/assets/time.svg";
import styled from "@stylesheet";

export const Wrapper = styled.section`
  min-height: 80vh;
  padding: 20px 0;
  background-color: ${({ theme }) => theme.palette.primary.main};
  display: flex;
  flex-direction: column;
`;

export const StyledContainer = styled(Container)`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

export const Heading = styled(Typography)`
  text-align: center;
  font-family: Simplifica;
  font-size: 3rem;
  font-weight: bold;
  letter-spacing: 1px;
  text-transform: lowercase;
  padding: 30px 0;
  color: ${({ theme }) => theme.palette.common.white};
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  flex: 1;
  margin: 30px 0;
`;

interface IsEven {
  isEven?: boolean;
}

export const ContentItem = styled.div<IsEven>`
  display: flex;
  align-self: ${({ isEven }) => (isEven ? "flex-start" : "flex-end")};
  align-items: center;
  justify-content: ${({ isEven }) => (isEven ? "flex-end" : "")};
  width: calc(50% + 40px);

  ${({ theme }) => `
    ${theme.breakpoints.down("sm")} {
      width: calc(50% + 25px);
    }
  `}
`;

export const Text = styled.p<IsEven>`
  color: ${({ theme }) => theme.palette.common.white};
  margin: ${({ isEven }) => (isEven ? "0 80px 0 0" : "0 0 0 80px")};
  max-width: 350px;
  word-wrap: break-word;
  font-size: 1.4rem;
  font-family: ${({ theme }) => theme.typography.fontFamily};

  ${({ theme, isEven }) => `
    ${theme.breakpoints.down("sm")} {
      width: calc(100% - 40px);
      margin: ${isEven ? "0 40px 0 0" : "0 0 0 40px"};    
    }
  `}

  ${({ theme }) => `
    ${theme.breakpoints.down("xs")} {
      width: calc(100% - 20px);
    }
  `}
`;

const IconStyles = css`
  flex-shrink: 0;

  @media only screen and (max-width: 960px) {
    max-width: 50px;
  }
`;

export const Time = styled(TimeIcon)`
  ${IconStyles}
`;
export const Security = styled(SecurityIcon)`
  ${IconStyles}
`;
export const Notification = styled(NotificationIcon)`
  ${IconStyles}
`;
export const Document = styled(DocumentIcon)`
  ${IconStyles}
`;

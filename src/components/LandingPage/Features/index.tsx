import React from "react";
import {
  Content,
  ContentItem,
  Document,
  Heading,
  Notification,
  Security,
  StyledContainer,
  Text,
  Time,
  Wrapper,
} from "./styled";

export default () => (
  <Wrapper id="features">
    <StyledContainer maxWidth="lg">
      <Heading variant="h6" variantMapping={{ h6: "h3" }}>
        Features
      </Heading>
      <Content>
        <ContentItem isEven>
          <Text isEven>
            You can be sure your scheduled content is safe. The person for which the content is
            scheduled, will receive uniquely generated password. That allows only them to access the
            content.
          </Text>
          <Security />
        </ContentItem>
        <ContentItem>
          <Notification />
          <Text>
            A reliable service takes care of notifying the person on scheduled dates with 1 minute
            of maximum deviation!
          </Text>
        </ContentItem>
        <ContentItem isEven>
          <Text isEven>
            Set up up to 6 notifications before the content actually becomes available for viewing.
          </Text>
          <Time />
        </ContentItem>
        <ContentItem>
          <Document />
          <Text>
            You can attach any type of file to your scheduled content. The typical use case is a
            photo or a video, but PDF document works too!
          </Text>
        </ContentItem>
      </Content>
    </StyledContainer>
  </Wrapper>
);

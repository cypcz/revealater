import { User } from "@context/Authentication";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import { Routes } from "@utils/routes";
import { navigate } from "gatsby";
import React from "react";
import { BoldText, BuyButton, Card, CardHeading, Content, LoginButton, Price } from "./styled";

interface Props {
  user: User | null;
}

export default ({ user }: Props) => (
  <Content>
    <Card>
      <CardHeading variant="body1">Basic</CardHeading>
      <ul>
        <Typography variant="body2" component="li">
          anonymous scheduling
        </Typography>
        <Typography variant="body2" component="li">
          1 notification
        </Typography>
        <Typography variant="body2" component="li">
          2000 message characters
        </Typography>
      </ul>
      {!user && <BoldText variant="body1">Log in and get:</BoldText>}
      <ul>
        <Typography variant="body2" component="li">
          named scheduling
        </Typography>
        <Typography variant="body2" component="li">
          5000 message characters
        </Typography>
      </ul>
      {!user && (
        <LoginButton variant="contained" color="primary" onClick={() => navigate(Routes.APP)}>
          Log in
        </LoginButton>
      )}
    </Card>
    <Card>
      <CardHeading variant="body1">Pro</CardHeading>
      <Price variant="body2" align="center">
        only €1.<sup>95</sup>/month
      </Price>
      <BoldText variant="body1">Basic plus:</BoldText>
      <ul>
        <Typography variant="body2" component="li">
          up to 6 notifications
        </Typography>
        <Typography variant="body2" component="li">
          file upload up to 1 GB
        </Typography>
      </ul>
      {!user?.isSubscribed && (
        <BuyButton onClick={() => navigate(Routes.CHECKOUT)}>Go for it</BuyButton>
      )}
    </Card>
    <Card>
      <CardHeading variant="body1">Individual</CardHeading>
      <Typography variant="body2" style={{ margin: "0 20px" }}>
        Looking for a custom upload quota or a new feature? Get in touch with us and let's figure it
        out!
      </Typography>
      <Link href="mailto:revealater.com">
        <LoginButton variant="contained" color="primary" style={{ marginTop: "115px" }}>
          Contact us
        </LoginButton>
      </Link>
    </Card>
  </Content>
);

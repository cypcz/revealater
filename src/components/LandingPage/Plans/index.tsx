import React from "react";
import Cards from "./cards";
import { Heading, StyledContainer, Wrapper } from "./styled";

export default () => (
  <Wrapper id="plans">
    <StyledContainer maxWidth="lg">
      <Heading variant="h6" variantMapping={{ h6: "h3" }}>
        Plans
      </Heading>
      <Cards user={null} />
    </StyledContainer>
  </Wrapper>
);

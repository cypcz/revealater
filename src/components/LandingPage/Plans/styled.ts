import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import styled from "@stylesheet";

export const Wrapper = styled.section`
  min-height: 80vh;
  padding: 20px 0;
  background-color: ${({ theme }) => theme.palette.common.white};
  display: flex;
  flex-direction: column;
`;

export const StyledContainer = styled(Container)`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

export const Heading = styled(Typography)`
  text-align: center;
  font-family: Simplifica;
  font-size: 3rem;
  font-weight: bold;
  letter-spacing: 1px;
  text-transform: lowercase;
  padding: 30px 0;
`;

export const Content = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1;
  margin: 30px 0;

  ${({ theme }) => `
    ${theme.breakpoints.down("sm")} {
      flex-direction: column;
    }
  `}
`;

export const Card = styled.div`
  width: 330px;
  height: 300px;
  background-color: ${({ theme }) => theme.palette.grey[200]};
  border-radius: 12px;
  padding: 30px 0;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  :nth-of-type(2) {
    background: -webkit-linear-gradient(top, #38bb35, #38bb35 30px, white 30px, white);
    height: 360px;
    padding-top: 60px;
  }

  ${({ theme }) => `
    ${theme.breakpoints.down("sm")} {
      margin: 10px 0;
    }
  `}
`;

export const CardHeading = styled(Typography)`
  font-family: Simplifica;
  font-size: 2.4rem;
  font-weight: bold;
  text-transform: lowercase;
  text-align: center;
  margin-bottom: 10px;
`;

export const LoginButton = styled(Button)`
  display: flex;
  margin: 23px auto 0 auto;
  position: initial;
`;

export const BuyButton = styled(Button)`
  display: flex;
  margin: 90px auto 0 auto;
  background-color: rgb(56, 187, 53);
  position: initial;
  width: 100px;
  :hover {
    background-color: rgb(56, 175, 53);
  }
`;

export const Price = styled(Typography)`
  font-style: italic;
`;

export const BoldText = styled(Typography)`
  font-weight: bold;
  margin: 20px 0 10px 20px;
`;

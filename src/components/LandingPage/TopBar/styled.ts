import { css } from "@emotion/core";
import Button from "@material-ui/core/Button";
import Logo from "@static/assets/logo.svg";
import styled from "@stylesheet";
import { Link } from "react-scroll";

interface Wrapper {
  isScrolled: boolean;
}

export const Wrapper = styled.header<Wrapper>`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 80px;
  width: 100%;
  padding: 0 60px;
  position: fixed;
  top: 0;
  background-color: ${({ isScrolled, theme }) => (isScrolled ? theme.palette.grey[200] : "")};
  box-shadow: ${({ isScrolled, theme }) =>
    isScrolled ? `0px 1px 5px 0px ${theme.palette.text.primary}` : ""};
  transition: 0.3s all;
  z-index: 1000;
`;

export const LogoPart = styled.div`
  display: flex;
  :hover {
    cursor: pointer;
  }
`;

export const LogoIcon = styled(Logo)`
  margin-right: 20px;
`;

export const LogoHeading = styled.p`
  font-family: Sriracha;
  font-size: 2rem;
  margin: auto 0;
  text-transform: uppercase;
  color: ${({ theme }) => theme.palette.text.primary};

  ${({ theme }) => `
    ${theme.breakpoints.down("xs")} {
      display: none;
    }
  `}
`;

export const NavigationPart = styled.nav`
  display: flex;

  ${({ theme }) => `
    ${theme.breakpoints.down("sm")} {
      display: none;
    }
  `}
`;

const LinkStyle = css`
  font-family: Simplifica;
  font-size: 2rem;
  letter-spacing: 1px;
  font-weight: bold;
  text-transform: lowercase;
`;

export const NavButton = styled(Button)`
  ${LinkStyle}
  margin-right: 20px;
  :last-child {
    margin-right: 0;
  }
`;

export const OverlayNavButton = styled(Button)`
  ${LinkStyle}
  color: ${({ theme }) => theme.palette.common.white};
`;

export const StyledLink = styled(Link)`
  margin-right: 20px;
`;

interface OpenState {
  isOpen: boolean;
}

export const Hamburger = styled.div<OpenState>`
  cursor: pointer;
  width: 30px;
  height: 30px;
  display: flex;
  flex-direction: column;
  flex-shrink: 0;
  justify-content: ${props => (props.isOpen ? "center" : "space-around")};
  transform: ${props => (props.isOpen ? "rotate(135deg)" : "")};
  transition: all 0.4s ease;
  z-index: 2;

  ${({ theme }) => `
    ${theme.breakpoints.up("md")} {
      display: none;
    }
  `}
`;

export const HamburgerLine = styled.div<OpenState>`
  height: 2px;
  width: 100%;
  background-color: ${({ theme, isOpen }) =>
    isOpen ? theme.palette.common.white : theme.palette.text.primary};
  transform: ${props => (props.isOpen ? "rotate(90deg)" : "")};
  :first-of-type {
    transform: ${props => (props.isOpen ? "rotate(180deg) translateY(-2px)" : "")};
  }
  :last-of-type {
    display: ${props => (props.isOpen ? "none" : "")};
  }
`;

export const OverlayMenu = styled.div<OpenState>`
  visibility: ${({ isOpen }) => (isOpen ? "visible" : "hidden")};
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 1;
  background-color: ${({ theme }) => theme.palette.primary.main};
  cursor: default;
  opacity: ${({ isOpen }) => (isOpen ? 0.98 : 0)};
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
  transition: all 0.3s;

  ${({ theme }) => `
    ${theme.breakpoints.up("md")} {
      display: none;
    }
  `}
`;

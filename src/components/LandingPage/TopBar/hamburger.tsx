import { Routes } from "@utils/routes";
import { navigate } from "gatsby";
import React, { useState } from "react";
import { Link } from "react-scroll";
import { Hamburger, HamburgerLine, OverlayMenu, OverlayNavButton } from "./styled";

export default () => {
  const [open, toggleOpen] = useState(false);

  return (
    <>
      <Hamburger isOpen={open} onClick={() => toggleOpen(!open)}>
        <HamburgerLine isOpen={open} />
        <HamburgerLine isOpen={open} />
        <HamburgerLine isOpen={open} />
      </Hamburger>
      <OverlayMenu isOpen={open}>
        <Link to="howitworks" smooth offset={-60}>
          <OverlayNavButton onClick={() => toggleOpen(!open)}>How it works</OverlayNavButton>
        </Link>
        <Link to="features" smooth offset={-60}>
          <OverlayNavButton onClick={() => toggleOpen(!open)}>Features</OverlayNavButton>
        </Link>
        <Link to="plans" smooth offset={-60}>
          <OverlayNavButton onClick={() => toggleOpen(!open)}>Plans</OverlayNavButton>
        </Link>
        <OverlayNavButton onClick={() => navigate(Routes.APP)}>Log in</OverlayNavButton>
        <OverlayNavButton onClick={() => navigate(Routes.APP, { state: { signup: true } })}>
          Sign up
        </OverlayNavButton>
      </OverlayMenu>
    </>
  );
};

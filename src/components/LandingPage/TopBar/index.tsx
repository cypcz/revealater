import { Routes } from "@utils/routes";
import { navigate } from "gatsby";
import React, { useEffect, useState } from "react";
import { animateScroll } from "react-scroll";
import Hamburger from "./hamburger";
import {
  LogoHeading,
  LogoIcon,
  LogoPart,
  NavButton,
  NavigationPart,
  StyledLink,
  Wrapper,
} from "./styled";

export default () => {
  const [scrolled, toggleScrolled] = useState(false);

  const listenScrollEvent = () => {
    if (window.scrollY > 20) {
      toggleScrolled(true);
    } else {
      toggleScrolled(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", listenScrollEvent);

    return () => {
      window.removeEventListener("scroll", listenScrollEvent);
    };
  }, []);

  return (
    <Wrapper isScrolled={scrolled}>
      <LogoPart onClick={() => animateScroll.scrollToTop()}>
        <LogoIcon />
        <LogoHeading>Revealater</LogoHeading>
      </LogoPart>
      <Hamburger />
      <NavigationPart>
        <StyledLink to="howitworks" smooth offset={-60}>
          <NavButton>How it works</NavButton>
        </StyledLink>
        <StyledLink to="features" smooth offset={-60}>
          <NavButton>Features</NavButton>
        </StyledLink>
        <StyledLink to="plans" smooth offset={-60}>
          <NavButton>Plans</NavButton>
        </StyledLink>
        <NavButton onClick={() => navigate(Routes.APP)}>Log in</NavButton>
        <NavButton onClick={() => navigate(Routes.APP, { state: { signup: true } })}>
          Sign up
        </NavButton>
      </NavigationPart>
    </Wrapper>
  );
};

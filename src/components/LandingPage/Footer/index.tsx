import Link from "@material-ui/core/Link";
import getYear from "date-fns/getYear";
import React from "react";
import {
  Contact,
  ContactText,
  ContactTextBold,
  Content,
  CopyrightText,
  Links,
  LinksColumn,
  StyledContainer,
  StyledLink,
  Wrapper,
} from "./styled";

export default () => (
  <Wrapper>
    <StyledContainer maxWidth="lg">
      <Content>
        <Links>
          <LinksColumn>
            {/*             <StyledLink to="/faq">FAQ</StyledLink>
             */}
            <StyledLink to="/privacy">Privacy Policy</StyledLink>
            <StyledLink to="/terms">Terms of Service</StyledLink>
          </LinksColumn>
        </Links>
        <Contact>
          <ContactText>Found a bug or just want to reach out?</ContactText>
          <ContactTextBold>
            <Link href="mailto:hello@revealater.com" color="textPrimary" underline="none">
              hello@revealater.com
            </Link>
          </ContactTextBold>
        </Contact>
      </Content>
      <CopyrightText variant="body2">{`Copyright © ${getYear(
        new Date()
      )} Jakub Kot. All rights reserved.`}</CopyrightText>
    </StyledContainer>
  </Wrapper>
);

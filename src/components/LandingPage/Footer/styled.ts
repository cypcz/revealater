import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import styled from "@stylesheet";
import { Link } from "gatsby";

export const Wrapper = styled.section`
  min-height: 230px;
  background-color: ${({ theme }) => theme.palette.grey[200]};
`;

export const StyledContainer = styled(Container)`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

export const Content = styled.div`
  display: flex;
  justify-content: space-between;
  flex: 1;
  margin-top: 30px;
  margin-bottom: 35px;

  ${({ theme }) => `
    ${theme.breakpoints.down("sm")} {
      flex-direction: column;
    }
  `}
`;

export const Links = styled.div`
  display: flex;
  margin-bottom: 20px;

  ${({ theme }) => `
    ${theme.breakpoints.down("sm")} {
      justify-content: center;
    }
  `}
`;

export const LinksColumn = styled.div`
  display: flex;
  flex-direction: column;

  ${({ theme }) => `
    ${theme.breakpoints.down("sm")} {
      text-align: center;
    }
  `}
    :nth-of-type(2) {
    margin-left: 100px;
  }
`;

export const Contact = styled.div``;

export const ContactText = styled(Typography)`
  font-size: 1.8rem;
  margin-bottom: 40px;
  text-align: center;
`;

export const ContactTextBold = styled(Typography)`
  font-size: 1.8rem;
  font-weight: bold;
  text-align: center;
`;

export const CopyrightText = styled(Typography)`
  text-align: center;
  margin: 5px 0;
`;

export const StyledLink = styled(Link)`
  color: ${({ theme }) => theme.palette.text.primary};
  font-family: ${({ theme }) => theme.typography.fontFamily};
  text-decoration: none;
  font-weight: bold;
  font-size: 1.8rem;
  margin-bottom: 20px;
`;

import { graphql, StaticQuery } from "gatsby";
import BackgroundImage from "gatsby-background-image";
import React from "react";

interface Props {
  children: JSX.Element | JSX.Element[];
}

export default ({ children }: Props) => (
  <StaticQuery
    query={graphql`
      query {
        desktop: file(relativePath: { eq: "landing.jpg" }) {
          childImageSharp {
            fluid(quality: 90, maxWidth: 1920) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
    `}
    render={data => {
      const imageData = [
        data.desktop.childImageSharp.fluid,
        `linear-gradient(rgba(255,255,255, 0.8), rgba(255,255,255,0.8))`,
      ].reverse();
      return (
        <BackgroundImage Tag="section" fluid={imageData}>
          {children}
        </BackgroundImage>
      );
    }}
  />
);

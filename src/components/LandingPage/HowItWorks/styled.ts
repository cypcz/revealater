import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import ArrowDownImg from "@static/assets/arrow_down.svg";
import ArrowRightImg from "@static/assets/arrow_right.svg";
import FemaleImg from "@static/assets/female.svg";
import FillFormImg from "@static/assets/fill_form.svg";
import GiftImg from "@static/assets/gift.svg";
import styled from "@stylesheet";

export const Wrapper = styled.section`
  min-height: 80vh;
  padding: 20px 0;
  background-color: ${({ theme }) => theme.palette.grey[200]};
  display: flex;
  flex-direction: column;
`;

export const StyledContainer = styled(Container)`
  flex: 1;
  display: flex;
  flex-direction: column;
  margin-bottom: 20px;
`;

export const Heading = styled(Typography)`
  font-family: Simplifica;
  font-size: 3rem;
  font-weight: bold;
  letter-spacing: 1px;
  text-transform: lowercase;
  text-align: center;
  padding: 30px 0;
`;

export const Diagram = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;

  ${({ theme }) => `
    ${theme.breakpoints.down("sm")} {
      flex-direction: column;
    }
  `}
`;

export const DiagramItem = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const DiagramText = styled(Typography)`
  text-align: center;
  padding-top: 30px;
  max-width: 250px;

  ${({ theme }) => `
    ${theme.breakpoints.down("sm")} {
      padding-top: 0;
    }
  `}
`;

export const Female = styled(FemaleImg)`
  max-width: 250px;
  margin-bottom: 5px;
`;

export const FillForm = styled(FillFormImg)`
  max-width: 250px;
`;

export const Gift = styled(GiftImg)`
  max-width: 250px;
`;

export const ArrowRight = styled(ArrowRightImg)`
  max-width: 150px;
`;

export const ArrowDown = styled(ArrowDownImg)`
  max-height: 70px;
  margin: 30px 0;
`;

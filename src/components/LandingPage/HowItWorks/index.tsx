import useMediaQuery from "@material-ui/core/useMediaQuery";
import React from "react";
import {
  ArrowDown,
  ArrowRight,
  Diagram,
  DiagramItem,
  DiagramText,
  Female,
  FillForm,
  Gift,
  Heading,
  StyledContainer,
  Wrapper,
} from "./styled";

export default () => {
  const isLessThan960 = useMediaQuery("(max-width:960px)");

  return (
    <Wrapper id="howitworks">
      <StyledContainer maxWidth="lg">
        <Heading variant="h6" variantMapping={{ h6: "h3" }}>
          How it works
        </Heading>
        <Diagram>
          <DiagramItem>
            <Female />
            <DiagramText variant="body2">
              You've got a content that you want to present to somebody anytime in the future. It
              can be in 2 days, in a month or in few years. <br /> <br />
              It can be a birthday wish, a reminder of what you did together few years ago, a will
              for your kids. Or anything else that you can think of..
            </DiagramText>
          </DiagramItem>
          {isLessThan960 ? <ArrowDown /> : <ArrowRight />}
          <DiagramItem>
            <FillForm />
            <DiagramText variant="body2">
              Fill in the person's email address, a message and/or a file, then pick the dates when
              you want the notifications to happen. Only the last notification makes the content
              available. The previous notifications serve the purpose of building the EXCITEMENT for
              the scheduled content.
            </DiagramText>
          </DiagramItem>
          {isLessThan960 ? <ArrowDown /> : <ArrowRight />}
          <DiagramItem>
            <Gift />
            <DiagramText variant="body2">
              The person will be notified on the dates specified by yourself, and after the last
              notification, they can access the content!
            </DiagramText>
          </DiagramItem>
        </Diagram>
      </StyledContainer>
    </Wrapper>
  );
};

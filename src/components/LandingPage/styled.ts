import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import styled from "@stylesheet";

export const Wrapper = styled.div`
  min-height: 80vh;
`;

export const MarginedGrid = styled(Grid)`
  padding: 80px 0;
`;

export const StyledTypography = styled(Typography)`
  margin: 15px 0;
  font-family: Simplifica;
  font-size: 5rem;
  text-align: center;
`;

export const StyledButton = styled(Button)`
  margin: 20px 0;
  height: 50px;
`;

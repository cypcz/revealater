import Button from "@material-ui/core/Button";
import Fab from "@material-ui/core/Fab";
import styled from "@stylesheet";
import { Form } from "formik";
import { Link } from "gatsby";

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;

export const StyledForm = styled(Form)`
  max-width: 600px;
`;

export const ButtonGroup = styled.div`
  width: 100%;
  display: flex;
`;

export const StyledButton = styled(Button)`
  margin-top: 30px;
  height: 50px;
  :first-of-type {
    margin-right: 10px;
    flex: 2;
  }
  :last-of-type {
    margin-left: 10px;
    flex: 1;
    color: ${({ theme }) => theme.palette.text.primary};
  }
`;

export const DateRow = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  flex-wrap: wrap;
`;

export const DateWithIcon = styled.div<{ isOdd: boolean }>`
  display: flex;
  align-items: center;
  width: ${({ isOdd }) => (isOdd ? "100%" : "48%")};
`;

export const RemoveFab = styled(Fab)`
  margin-left: 5px;
  margin-top: 8px;
`;

export const StyledFab = styled(Fab)`
  display: flex;
  margin: 10px auto;
`;

export const StyledLink = styled(Link)`
  color: ${({ theme }) => theme.palette.primary.main};
  text-decoration: none;

  :visited {
    color: ${({ theme }) => theme.palette.primary.main};
  }
`;

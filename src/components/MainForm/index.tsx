import DateTimeInput from "@components/FormInputs/DateTimeInput";
import TextInput from "@components/FormInputs/TextInput";
import UploadInput from "@components/FormInputs/UploadInput";
import { User } from "@context/Authentication";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import { Routes } from "@utils/routes";
import firebase from "firebase/app";
import "firebase/functions";
import "firebase/storage";
import { FieldArray, Formik, FormikHelpers } from "formik";
import { navigate } from "gatsby";
import { useSnackbar } from "notistack";
import React, { useState } from "react";
import * as Yup from "yup";
import {
  ButtonGroup,
  DateRow,
  DateWithIcon,
  RemoveFab,
  StyledButton,
  StyledFab,
  StyledForm,
  StyledLink,
  Wrapper,
} from "./styled";

const validationSchema = Yup.object().shape({
  message: Yup.string().required("This field is required"),
  to: Yup.string()
    .email("Please enter valid email address")
    .required("This field is required"),
  from: Yup.string(),
  file: Yup.mixed().test("file", "File cannot be larger than 1GB", value => {
    if (value) return value.size <= 1073741824;
    else return true;
  }),
});

const DEV = process.env.NODE_ENV === "development";

interface FormValues {
  message: string;
  to: string;
  from: string;
  subject: string;
  notifyOn: Date[];
  file: File | null;
}

interface Props {
  user: User | null;
}

const MainForm = ({ user }: Props) => {
  const [submitting, setSubmitting] = useState(false);
  const [uploadProgress, setUploadProgress] = useState(0);
  const { enqueueSnackbar } = useSnackbar();

  const notificationLimit = user?.isPro ? 6 : 0;

  const handleSubmit = async (values: FormValues, { resetForm }: FormikHelpers<FormValues>) => {
    setSubmitting(true);

    if (DEV) {
      firebase.functions().useFunctionsEmulator("http://localhost:5000");
    }

    const createContent = async (fileUrl?: string) => {
      const { data } = await firebase
        .app()
        .functions(DEV ? undefined : "europe-west1")
        .httpsCallable("createContent")({
        notifyOn: values.notifyOn
          .sort((a, b) => a.getTime() - b.getTime())
          .map(date => date.toISOString()),
        from: values.from,
        to: values.to,
        message: values.message,
        subject: values.subject,
        fileUrl,
      });

      resetForm();
      setSubmitting(false);

      if (data.error) {
        enqueueSnackbar(<Typography>An error occured while scheduling your content.</Typography>, {
          autoHideDuration: 3000,
          variant: "error",
        });
      } else {
        enqueueSnackbar(<Typography>Your message for {values.to} is scheduled</Typography>, {
          autoHideDuration: 3000,
          variant: "success",
        });
      }
    };

    if (values.file?.size) {
      const uploadTask = firebase
        .storage()
        .ref()
        .child(`${Date.now()}_${values.file.name}`)
        .put(values.file);

      uploadTask.on(
        "state_changed",
        snapshot => {
          const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          setUploadProgress(Math.round(progress));
        },
        error => {
          console.log(error);
        },
        async () => {
          const fileUrl = await uploadTask.snapshot.ref.getDownloadURL();
          await createContent(fileUrl);
          setUploadProgress(0);
        }
      );
    } else {
      createContent();
    }
  };

  return (
    <Wrapper>
      <Formik<FormValues>
        initialValues={{
          message: "",
          to: "",
          from: user ? user.email! : "",
          subject: "",
          notifyOn: [new Date()],
          file: null,
        }}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ values }) => (
          <StyledForm>
            {user && (
              <TextInput
                id="from"
                label="From"
                name="from"
                placeholder="Name or email"
                margin="normal"
                size="small"
                fullWidth
              />
            )}
            <TextInput
              id="to"
              label="To"
              name="to"
              placeholder="Email"
              margin="normal"
              size={user ? "small" : undefined}
              fullWidth
            />
            <TextInput
              id="subject"
              label="Subject"
              name="subject"
              margin="normal"
              size={user ? "small" : undefined}
              fullWidth
            />
            <TextInput
              id="message"
              label="Message"
              name="message"
              margin="normal"
              size={user ? "small" : undefined}
              fullWidth
              multiline
              rows={3}
            />
            <FieldArray
              name="notifyOn"
              render={({ push, remove }) => (
                <>
                  <DateRow>
                    {values.notifyOn.map((date, index) => (
                      <DateWithIcon key={index} isOdd={!user && values.notifyOn.length === 1}>
                        <DateTimeInput
                          id={`notifyOn.${index}`}
                          label={`${index + 1}. notification on`}
                          name={`notifyOn.${index}`}
                          margin="normal"
                          size={user ? "small" : undefined}
                          style={{ flex: 1 }}
                        />
                        {!!notificationLimit && values.notifyOn.length !== 1 && (
                          <RemoveFab onClick={() => remove(index)} size="small" color="secondary">
                            <RemoveIcon />
                          </RemoveFab>
                        )}
                      </DateWithIcon>
                    ))}
                  </DateRow>
                  {values.notifyOn.length < notificationLimit && (
                    <StyledFab color="primary" size="small" onClick={() => push(new Date())}>
                      <AddIcon />
                    </StyledFab>
                  )}
                </>
              )}
            />
            {user?.isPro && <UploadInput name="file" progress={uploadProgress} />}
            {user ? (
              <Button
                type="submit"
                variant="contained"
                color="primary"
                style={{ margin: "20px 0" }}
                disabled={submitting}
                fullWidth
              >
                Schedule
              </Button>
            ) : (
              <>
                <Typography variant="subtitle2">
                  By using the service, you agree to our{" "}
                  <StyledLink to={Routes.TERMS}>terms of service</StyledLink> and{" "}
                  <StyledLink to={Routes.PRIVACY}>privacy policy</StyledLink>.
                </Typography>
                <ButtonGroup>
                  <StyledButton
                    type="submit"
                    variant="contained"
                    color="primary"
                    disabled={submitting}
                  >
                    Schedule anonymously
                  </StyledButton>
                  <StyledButton onClick={() => navigate(Routes.APP)} variant="outlined">
                    Sign up
                  </StyledButton>
                </ButtonGroup>
              </>
            )}
          </StyledForm>
        )}
      </Formik>
    </Wrapper>
  );
};

export default MainForm;

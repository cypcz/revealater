import { User } from "@context/Authentication";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import {
  CardElement,
  PaymentRequestButtonElement,
  useElements,
  useStripe,
} from "@stripe/react-stripe-js";
import { PaymentMethod, PaymentRequest } from "@stripe/stripe-js";
import { theme } from "@stylesheet";
import firebase from "firebase/app";
import "firebase/functions";
import { useSnackbar } from "notistack";
import React, { useEffect, useState } from "react";
import { Wrapper } from "./styled";

const DEV = process.env.NODE_ENV === "development";

const style = {
  base: {
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamily,
    fontSmoothing: "antialiased",
    fontSize: "16px",
    "::placeholder": {
      color: theme.palette.grey[500],
    },
  },
  invalid: {
    color: theme.palette.error.main,
    iconColor: "#fa755a",
  },
};

type State = PaymentRequest | null;

interface Props {
  user?: User | null;
}

const SubscriptionForm = ({ user }: Props) => {
  const [paymentRequest, setPaymentRequest] = useState<State>(null);
  const [loading, setLoading] = useState(false);
  const stripe = useStripe();
  const elements = useElements();
  const { enqueueSnackbar } = useSnackbar();

  const createSubscription = async (paymentMethod: PaymentMethod) => {
    if (DEV) {
      firebase.functions().useFunctionsEmulator("http://localhost:5000");
    }

    const { data } = await firebase
      .app()
      .functions(DEV ? undefined : "europe-west1")
      .httpsCallable("createSubscription")({
      paymentMethod,
    });

    const { latest_invoice } = data.data.subscription;
    const { payment_intent } = latest_invoice;

    if (payment_intent) {
      const { client_secret, status } = payment_intent;

      if (status === "requires_action") {
        stripe!.confirmCardPayment(client_secret).then(async result => {
          if (result.error) {
            setLoading(false);
            enqueueSnackbar(<Typography>{result.error.message}</Typography>, {
              autoHideDuration: 3000,
              variant: "error",
            });
          } else {
            setLoading(false);
            enqueueSnackbar(<Typography>Subscription created successfuly! Thank you.</Typography>, {
              autoHideDuration: 3000,
              variant: "success",
            });
          }
        });
      } else {
        setLoading(false);
        enqueueSnackbar(<Typography>Subscription created successfuly! Thank you.</Typography>, {
          autoHideDuration: 3000,
          variant: "success",
        });
      }
    }
  };

  useEffect(() => {
    if (!stripe || !elements) {
      return;
    }

    const paymentRequest = stripe.paymentRequest({
      country: "CZ",
      currency: "eur",
      total: {
        label: "Revealater PRO membership",
        amount: 195,
      },
      requestPayerName: true,
      requestPayerEmail: true,
    });

    paymentRequest.on("paymentmethod", async ev => {
      try {
        await createSubscription(ev.paymentMethod);
        ev.complete("success");
      } catch (error) {
        ev.complete("fail");
      }
    });

    paymentRequest.canMakePayment().then(result => {
      if (result) {
        setPaymentRequest(paymentRequest);
      } else {
        console.log("cannot make payment");
      }
    });
  }, [stripe, elements]);

  const handleCardPayment = async () => {
    if (!stripe || !elements) {
      return;
    }
    setLoading(true);

    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: "card",
      card: elements.getElement(CardElement)!,
      billing_details: {
        email: user?.email!,
        name: user?.displayName!,
      },
    });

    if (error || !paymentMethod) {
      setLoading(false);
      enqueueSnackbar(<Typography>{error?.message}</Typography>, {
        autoHideDuration: 3000,
        variant: "error",
      });
    } else {
      await createSubscription(paymentMethod);
    }
  };

  return (
    <Wrapper>
      <Typography variant="body2" align="center">
        You are paying <b>€1.95</b> monthly for a Revealater <b>PRO</b> membership. <br /> You can
        cancel the subscription anytime.
      </Typography>
      <br />
      <br />
      <CardElement options={{ style, hidePostalCode: true, iconStyle: "solid" }} />
      <Button
        onClick={handleCardPayment}
        variant="contained"
        fullWidth
        color="primary"
        disabled={!stripe || loading}
        style={{ margin: "20px 0 10px 0" }}
      >
        Pay
      </Button>
      {paymentRequest && (
        <PaymentRequestButtonElement
          options={{
            paymentRequest,
          }}
        />
      )}
      <br />
      <Typography variant="subtitle2" align="center">
        Note: The payment usually takes up to 2 minutes to propagate. Please, refresh the page after
        that time.
      </Typography>
    </Wrapper>
  );
};

export default SubscriptionForm;

import styled from "@stylesheet";

export const Wrapper = styled.div`
  max-width: 600px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
`;

export const FormItem = styled.div`
  margin-bottom: 30px;
`;

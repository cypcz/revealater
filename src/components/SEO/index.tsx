import React from "react";
import Helmet from "react-helmet";

interface Props {
  description: string;
  title: string;
}

const SEO = ({ description, title }: Props) => {
  return (
    <Helmet
      title={title}
      titleTemplate={`%s | ${description}`}
      meta={[
        {
          name: `description`,
          content: description,
        },
        {
          property: `og:title`,
          content: title,
        },
        {
          property: `og:description`,
          content: description,
        },
        {
          property: `og:type`,
          content: `website`,
        },
        {
          name: `twitter:card`,
          content: `summary`,
        },
        {
          name: `twitter:title`,
          content: title,
        },
        {
          name: `twitter:description`,
          content: description,
        },
      ]}
    />
  );
};

export default SEO;

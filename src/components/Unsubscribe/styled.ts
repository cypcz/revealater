import styled from "@stylesheet";

export const Wrapper = styled.div`
  max-width: 600px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
`;

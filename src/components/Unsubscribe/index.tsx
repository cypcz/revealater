import { User } from "@context/Authentication";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import firebase from "firebase/app";
import "firebase/functions";
import { useSnackbar } from "notistack";
import React, { useState } from "react";
import { Wrapper } from "./styled";

const DEV = process.env.NODE_ENV === "development";

interface Props {
  user?: User | null;
}

const Unsubscribe = ({ user }: Props) => {
  const [loading, setLoading] = useState(false);
  const { enqueueSnackbar } = useSnackbar();
  const [showRefreshMsg, setShowRefreshMsg] = useState(false);

  const unsubscribe = async () => {
    if (DEV) {
      firebase.functions().useFunctionsEmulator("http://localhost:5000");
    }

    setLoading(true);
    const { data } = await firebase
      .app()
      .functions(DEV ? undefined : "europe-west1")
      .httpsCallable("cancelSubscription")();

    setLoading(false);

    if (data.error) {
      enqueueSnackbar(<Typography>{data.error.type}</Typography>, {
        autoHideDuration: 3000,
        variant: "error",
      });
    } else {
      setShowRefreshMsg(true);
      enqueueSnackbar(
        <Typography>
          Subscription canceled successfuly! Thank you for using our service.
        </Typography>,
        {
          autoHideDuration: 3000,
          variant: "success",
        }
      );
    }
  };

  return (
    <Wrapper>
      <Typography variant="body2" align="center">
        You are already subscribed to the service. Click the button below to unsubscribe.
      </Typography>
      <br />
      <br />
      <Button
        onClick={unsubscribe}
        variant="contained"
        fullWidth
        disabled={loading}
        color="secondary"
      >
        Unsubscribe
      </Button>
      <br />
      <br />
      {showRefreshMsg && (
        <Typography variant="subtitle2" align="center">
          Please, refresh the page.
        </Typography>
      )}
    </Wrapper>
  );
};

export default Unsubscribe;

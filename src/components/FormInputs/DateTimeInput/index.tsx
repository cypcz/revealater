import { DateTimePicker } from "@material-ui/pickers";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";
import { Field, FieldConfig, FieldProps, getIn } from "formik";
import React from "react";

interface Props {
  id: string;
  label?: string;
  fullWidth?: boolean;
  margin?: "none" | "normal" | "dense";
  style?: React.CSSProperties;
  size?: "small" | "medium";
}

const DateTimeInput = ({
  id,
  label,
  fullWidth,
  margin,
  form: { errors, touched, setFieldValue },
  field,
  size,
  style,
}: Props & FieldProps) => {
  const error = getIn(errors, field.name);
  const touch = getIn(touched, field.name);
  const handleChange = (value: MaterialUiPickersDate) => {
    setFieldValue(field.name, value);
  };
  return (
    <>
      <DateTimePicker
        id={id}
        value={field.value}
        onChange={handleChange}
        error={!!(touch && error)}
        label={label}
        fullWidth={fullWidth}
        ampm={false}
        size={size}
        variant="inline"
        inputVariant="outlined"
        disablePast
        margin={margin}
        helperText={touch && error ? error : ""}
        style={style}
      />
    </>
  );
};

export default (props: Omit<FieldConfig, "component"> & Props) => (
  <Field {...props} component={DateTimeInput} />
);

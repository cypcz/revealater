import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import { Field, FieldConfig, FieldProps, getIn } from "formik";
import React, { useState } from "react";

interface Props {
  id: string;
  required?: boolean;
  placeholder?: string;
  disabled?: boolean;
  fullWidth?: boolean;
  label?: string;
  multiline?: boolean;
  type?: string;
  rows?: number;
  size?: "small" | "medium";
  margin?: "none" | "normal" | "dense";
}

const TextInput = ({
  id,
  required,
  placeholder,
  disabled,
  fullWidth,
  label,
  margin,
  multiline,
  type,
  rows,
  size,
  form: { errors, touched },
  field,
}: Props & FieldProps) => {
  const [showPassword, togglePassword] = useState(false);
  const error = getIn(errors, field.name);
  const touch = getIn(touched, field.name);
  return (
    <>
      <TextField
        {...field}
        id={id}
        error={!!(touch && error)}
        label={label}
        placeholder={placeholder}
        margin={margin}
        multiline={multiline}
        variant="outlined"
        size={size}
        type={showPassword ? "text" : type}
        rows={rows}
        rowsMax={5}
        required={required}
        disabled={disabled}
        fullWidth={fullWidth}
        helperText={touch && error ? error : ""}
        InputLabelProps={{
          shrink: true,
        }}
        InputProps={{
          endAdornment:
            type === "password" ? (
              <InputAdornment position="end">
                <IconButton onClick={() => togglePassword(!showPassword)}>
                  {showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ) : null,
        }}
      />
    </>
  );
};

export default (props: Omit<FieldConfig, "component"> & Props) => (
  <Field {...props} component={TextInput} />
);

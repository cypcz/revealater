import DeleteIcon from "@material-ui/icons/Delete";
import styled from "@stylesheet";

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const UploadWrapper = styled.div<{ image?: string | null }>`
  background-color: ${({ theme }) => theme.palette.grey[300]};
  background-image: ${({ image }) => (image ? `url(${image})` : "")};
  background-size: cover;
  height: ${({ image }) => (image ? "160px" : "100px")};
  width: ${({ image }) => (image ? "160px" : "100%")};
  border: 1px solid ${({ theme }) => theme.palette.grey[500]};
  border-radius: ${({ image }) => (image ? "50%" : "4px")};
  margin: ${({ image }) => (image ? 0 : "20px 0 2px 0")};
  outline: none;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;

  :hover {
    cursor: pointer;
  }
`;

export const ErrorText = styled.span`
  color: ${({ theme }) => theme.palette.error.main};
`;

export const Delete = styled(DeleteIcon)`
  margin-left: 5px;
  :hover {
    cursor: pointer;
  }
`;

import Grid from "@material-ui/core/Grid";
import LinearProgress from "@material-ui/core/LinearProgress";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import { Field, FieldConfig, FieldProps, getIn } from "formik";
import React from "react";
import { useDropzone } from "react-dropzone";
import { Content, Delete, ErrorText, UploadWrapper } from "./styled";

interface Props {
  progress: number;
  image?: string | null;
}

const UploadInput = ({
  progress,
  image,
  form: { errors, setFieldValue, values },
  field: { name },
}: Props & FieldProps) => {
  const handleFileDrop = (files: File[]) => {
    setFieldValue(name, files[0]);
  };

  const handleFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (image) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files![0]);
      reader.onload = () => {
        setFieldValue("imagePreview", reader.result);
      };
    }

    setFieldValue(name, event.target.files![0]);
  };

  const handleDelete = () => {
    setFieldValue(name, null);
  };

  const { getRootProps, getInputProps } = useDropzone({
    onDrop: handleFileDrop,
    multiple: false,
    accept: image ? "image/*" : "*",
  });
  const error = getIn(errors, name);

  return (
    <Content>
      <UploadWrapper {...getRootProps()} image={image}>
        <input {...getInputProps()} onChange={handleFieldChange} />
        {!image && (
          <>
            <CloudUploadIcon fontSize="large" />
            Choose a file or drag it here
          </>
        )}
      </UploadWrapper>
      {!image && (
        <LinearProgress variant="determinate" value={progress} style={{ width: "100%" }} />
      )}
      {values[name] && !image && (
        <Grid container alignItems="center">
          {values[name].name}
          <Delete onClick={handleDelete} />
        </Grid>
      )}
      {error && <ErrorText>{error}</ErrorText>}
    </Content>
  );
};

export default (props: Omit<FieldConfig, "component"> & Props) => (
  <Field {...props} component={UploadInput} />
);

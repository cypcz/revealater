import DateFnsUtils from "@date-io/date-fns";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import { ThemeProvider } from "emotion-theming";
import firebase from "firebase/app";
import { SnackbarProvider } from "notistack";
import React from "react";
import { theme } from "./stylesheet";

interface Props {
  element: JSX.Element | JSX.Element[];
}

if (typeof window !== "undefined") {
  require("firebase/analytics");
  firebase.initializeApp(
    JSON.parse(Buffer.from(process.env.GATSBY_FIREBASE_CONFIG!, "base64").toString())
  );
  firebase.analytics();
}

const BaseApp = ({ element }: Props) => (
  <MuiThemeProvider theme={theme}>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <SnackbarProvider maxSnack={3}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>{element}</MuiPickersUtilsProvider>
      </SnackbarProvider>
    </ThemeProvider>
  </MuiThemeProvider>
);

export const wrapRootElement = ({ element }: Props) => <BaseApp element={element}></BaseApp>;
